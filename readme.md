# Laravel CRUD Controller for JSON APIs

## Including in an existing project

Add the following in your `composer.json` file

```
    "repositories": [
        {
            "type":"vcs",
            "url": "https://bitbucket.org/nick_voegeli/laravel-crud-controller.git"
        }
    ],
    "require": {
        "nickvoegeli/laravel-crud-controller": "dev-master"
    },
```

The controller is added to the `NickVoegeli\Http\Controllers` namespace, and will be usable with:

```
use NickVoegeli\Http\Controllers\CrudController;
```

Documentation of the controller's functionality is provided in the code.