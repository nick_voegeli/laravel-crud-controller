<?php

namespace NickVoegeli\Http\Controllers;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

abstract class CrudController extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * modelClass can be explicitly specified.  If it isn't, the controller will do its best to
	 * determine which model to used based on controller name.  For conventional names, like
	 * ThingController or ThingsController, the controller will use the Thing model, and there's
	 * no need to set this explicitly.
	 */
	protected $modelClass = null;

	// Model namespace can also be overridden
	protected $modelNamespace = 'App\Models\\';

	/**
	 * An array of eager-loaded relationships can be provided here to be loaded along with the
	 * list of results on a successful operation
	 */
	protected $eagerLoadedRelationships = [];

	/**
	 * If true, the API response will include a list of all records.  If false, the `payload` attribute
	 * on the response will contain the modified record only.
	 */
	protected $shouldReturnList = true;

	/**
	 * Get the fully-qualified model class name with namespace
	 * @return type
	 */
	public function getModelClass(): string {
		// If $this->modelClass is set, use it.
		// Otherwise, attempt to determine model name from the controller class name.
		// Standard controller naming - e.g., UsersController or UserController - will use User as the model
		// if no modelClass is set.
		// If you're using a class with an irregular plural - e.g. Activities - then specify
		// the model explicitly, or use the singular form for the controller name (ActivityController).
		if ($this->modelClass) return $this->modelNamespace . $this->modelClass;
		$controllerClass = (new \ReflectionClass($this))->getShortName();
		if (preg_match("/sController/", $controllerClass)) {
			return $this->modelNamespace . str_replace("sController", "", $controllerClass);
		} else if (preg_match("/Controller/", $controllerClass)) {
			return $this->modelNamespace . str_replace("Controller", "", $controllerClass);
		}
	}

	/**
	 * Get the name of the model, without namespace
	 * @return type
	 */
	protected function _getModelName(): string {
		return (new \ReflectionClass($this->getModelClass()))->getShortName();
	}

	/**
	 * Get a list of all records
	 * @return Response JSON
	 */
	public function index(Request $request): Response {
		try {
			return $this->_getSuccessResponse();
		} catch (Exception $e) {
			$this->_logException($e, $request);
			$response = ['success' => false];
			return $this->_response($response, 500);
		}
	}

	/**
	 * Get a specific record
	 * @return Response JSON
	 */
	public function show($id, Request $request): Response {
		try {
			$record = $this->getModelClass()::find($id);
			return $this->_getSuccessResponse($record);
		} catch (Exception $e) {
			$this->_logException($e, $request);
			$response = ['success' => false];
			return $this->_response($response, 404);
		}
	}

	/**
	 * Save a new record
	 * @return Response JSON
	 */
	public function store(Request $request): Response {
		try {
			$saveData = $request->all();
			if (isset($saveData['api_token'])) unset($saveData['api_token']);

			$relatedRecords = [];

			// If a relationship name is specified in eagerLoadedRelationships,
			// then it's likely that those related models will be sent back in
			// for a create or update call.  Handle this case automatically.

			// Store and unset related models so they don't throw a ColumnNotFound exception
			// during creation
			foreach($this->eagerLoadedRelationships as $relationship) {
				$field = $this->_camelToSnakeCase($relationship);

				if (isset($saveData[$field])) {
					$relatedRecords[$relationship] = $saveData[$field];
					unset($saveData[$field]);
				}
			}

			// Remove guarded fields from save data
			$modelClass = $this->getModelClass();
			$instance = new $modelClass;

			if ($instance->guarded) {
				foreach ($instance->guarded as $fieldName) {
					unset($saveData[$fieldName]);
				}
			}

			$record = $this->getModelClass()::create($saveData);

			$this->_updateOrCreateRelated($record, $relatedRecords);

			if ($this->shouldReturnList) {
				return $this->_getSuccessResponse();
			} else {
				return $this->_getSuccessResponse($record);
			}
		} catch (Exception $e) {
			$this->_logException($e, $request);
			$message = "Failed to update {$this->_getModelName()}: {$e->getMessage()}";
			$payload = ['trace' => $e->getTraceAsString()];
			$response = ['success' => false, 'message' => $message, 'payload' => $payload];
			return $this->_response($response, 500);
		}
	}

	/**
	 * Update a specific record
	 * @param int|string $id Record ID - strings are supported for Mongo etc.
	 * @return Response JSON
	 */
	public function update($id, Request $request): Response {
		try {
			$saveData = $request->all();

			if (isset($saveData['api_token'])) unset($saveData['api_token']);

			$relatedRecords = [];

			// If a relationship name is specified in eagerLoadedRelationships,
			// then it's likely that those related models will be sent back in
			// for a create or update call.  Handle this case automatically.

			// Store and unset related models so they don't throw a ColumnNotFound exception
			// during update
			foreach($this->eagerLoadedRelationships as $relationship) {
				$field = $this->_camelToSnakeCase($relationship);
				if (array_key_exists($field, $saveData)) {
					$relatedRecords[$relationship] = $saveData[$field];
					unset($saveData[$field]);
				}
			}

			$record = $this->getModelClass()::find($id);

			if (is_null($record)) {
				$response = ['success' => false, 'message' => "No {$this->_getModelName()} found with id $id"];
				return $this->_response($response, 400);
			}

			// Remove guarded fields from save data
			foreach ($record->guarded as $fieldName) {
				unset($saveData[$fieldName]);
			}

			$record->update($saveData);
			$this->_updateOrCreateRelated($record, $relatedRecords);
			
			if ($this->shouldReturnList) {
				return $this->_getSuccessResponse();
			} else {
				return $this->_getSuccessResponse($record);
			}
		} catch (Exception $e) {
			$this->_logException($e, $request);
			$message = "Failed to update {$this->_getModelName()}: {$e->getMessage()}";
			$payload = ['trace' => $e->getTraceAsString()];
			$response = ['success' => false, 'message' => $message, 'payload' => $payload];
			return $this->_response($response, 500);
		}
	}

	/**
	 * Destroy a specific record
	 * @param int|string $id Record ID - strings are supported for Mongo etc.
	 * @return Response JSON
	 */
	public function destroy($id, Request $request): Response {
		try {
			$record = $this->getModelClass()::find($id);

			if (is_null($record)) {
				$response = ['success' => false, 'message' => "No {$this->_getModelName()} found with id $id"];
				return $this->_response($response, 400);
			}

			$this->getModelClass()::destroy($id);
			return $this->_getSuccessResponse();
		} catch (Exception $e) {
			$this->_logException($e, $request);
			$message = "Failed to update {$this->_getModelName()}: {$e->getMessage()}";
			$payload = ['trace' => $e->getTraceAsString()];
			$response = ['success' => false, 'message' => $message, 'payload' => $payload];
			return $this->_response($response, 500);
		}
	}

	/**
	 * Update or create related records for a given record
	 * @param $record Main model instance being saved
	 * @param array $relatedRecords Array of associative arrays representing associated records, or flat array containing a single associate record
	 * @return null
	 */
	protected function _updateOrCreateRelated($record, array $relatedRecords) {
		// Create related models
		foreach($relatedRecords as $name => $relatedRecord) {
			// Delete existing records to start over
			$record->{$name}()->delete();
			if (!empty($relatedRecord)) {
				// If this is an array of records, as in a many:many, loop and save each
				if (isset($relatedRecord[0])) {
					foreach ($relatedRecord as $thisRecord) {
						$saved = $record->{$name}()->updateOrCreate($thisRecord);
					}
				} else {
					// If it's a single record, as in a 1:1, just save
					$saved = $record->{$name}()->updateOrCreate($relatedRecord);

					// If this is a belongsTo relationship, it's necessary to manually associate
					// the records.  In this case, the `associate` method will be defined on
					// the relationship.
					if (method_exists($record->{$name}(), 'associate')) {
						$associated = $record->{$name}()->associate($saved)->save();
					}
				}
			}
		}
	}

	/**
	 * Get JSON success response containing all records.  If you're working with large data sets
	 * or pagination, override this.
	 * @param mixed $record If `$this->shouldReturnList` is false, the affected record can be
	 *                      provided to this method to be returned to the user
	 * @return Response
	 */
	protected function _getSuccessResponse($record = null): Response {
		if ($record) {
			$response = ['success' => true, 'payload' => $record];
		} else if ($this->shouldReturnList) {
			$records = $this->getModelClass()::with($this->eagerLoadedRelationships)->get();
			$response = ['success' => true, 'payload' => ['list' => $records]];
		} else {
			$response = ['success' => true];
		}

		return $this->_response($response);
	}

	/**
	 * Build and return the JSON response
	 * @param array $response Array to be serialized as JSON for response body
	 * @param int|int $httpCode HTTP status code
	 * @return Response
	 */
	protected function _response(array $response, int $httpCode = 200): Response {
		return response(json_encode($response), $httpCode)->header('Content-Type', 'application/json');
	}

	/**
	 * Log an exception to the monolog
	 * @param Exception $e The exception to log
	 * @return void
	 */
	protected function _logException(Exception $e, Request $request) {
		$parameters = json_encode($request->all());
		$message = json_encode([
			'message' => $e->getMessage(),
			'file' => $e->getFile(),
			'line' => $e->getLine(),
			'request' => $parameters,
			'trace' => $e->getTrace()
		]);

		throw new Exception($message);
	}

	protected function _camelToSnakeCase($string) {
		return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
	}
}
